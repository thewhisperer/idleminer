﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdleMiner.GameClasses
{
	class VideoCard
	{
		/// <summary>
		/// Название видеокарты
		/// </summary>
		public string Name { get; set; }
		/// <summary>
		/// Производительность видеокарты в хэшах в секунду
		/// </summary>
		public int Performance { get; set; }
		/// <summary>
		/// Цена за видеокарту
		/// </summary>
		public int Price { get; set; }
		/// <summary>
		/// Год релиза видеокарты (когда поступает в продажу)
		/// </summary>
		public int Year { get; set; }
		/// <summary>
		/// Возраст видеокарты
		/// </summary>
		protected int Age { get; set; }
		/// <summary>
		/// Максимальный возраст видеокарты (когда выходит из строя)
		/// </summary>
		public int MaxAge { get; set; }
		/// <summary>
		/// Тепловыделение в Джоулях
		/// </summary>
		public int Thermo { get; set; }

		// override object.Equals
		public override bool Equals(object obj)
		{
			if (obj == null || GetType() != obj.GetType()) return false;

			// TODO: write your implementation of Equals() here
			if (((VideoCard)obj).Name == Name && ((VideoCard)obj).Performance == Performance) return true;
			else return false;
		}

        // override object.GetHashCode
        public override int GetHashCode()
        {
            // TODO: write your implementation of GetHashCode() here
            return base.GetHashCode();
        }

        public VideoCard(string name, int performance, int maxAge, int thermo, int year)
        {
            Name = name;
            Performance = performance;
            MaxAge = maxAge;
            Thermo = thermo;
            Year = year;
        }
	}
}
