﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdleMiner.GameClasses
{
	abstract class CryptoCurrency
	{
		/// <summary>
		/// Название криптовалюты
		/// </summary>
		public string Name { get; set; }
		/// <summary>
		/// Сложность вычисления хэша (множитель)
		/// </summary>
		public float HashHardness { get; set; } = 1f;
		/// <summary>
		/// Цена для продажи криптовалюты
		/// </summary>
		public int Price { get; set; }
	}
}
