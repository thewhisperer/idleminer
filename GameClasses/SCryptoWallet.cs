﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdleMiner.GameClasses
{
	struct SCryptoWallet
	{
		public CryptoCurrency CryptoCurrency { get; set; }
		public int Amount { get; set; }
	}
}
