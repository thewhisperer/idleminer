﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdleMiner.GameClasses.CryptoCurrencies
{
	class Bitcoin : CryptoCurrency
	{
		public Bitcoin()
		{
			Name = "BitCoin";
			Price = 1000;
			HashHardness = 10f;
		}
	}
}
