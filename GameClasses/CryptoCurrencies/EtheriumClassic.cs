﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdleMiner.GameClasses.CryptoCurrencies
{
	class EtheriumClassic : CryptoCurrency
	{
		public EtheriumClassic()
		{
			Name = "Etherium Classic";
			Price = 250;
			HashHardness = 2.5f;
		}
	}
}
