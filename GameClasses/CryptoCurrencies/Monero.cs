﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdleMiner.GameClasses.CryptoCurrencies
{
	class Monero : CryptoCurrency
	{
		public Monero()
		{
			Name = "Monero";
			Price = 10;
			HashHardness = 0.5f;
		}
	}
}
