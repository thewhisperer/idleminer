﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdleMiner.GameClasses.CryptoCurrencies
{
	class Etherium : CryptoCurrency
	{
		public Etherium()
		{
			Name = "Etherium";
			Price = 500;
			HashHardness = 3f;
		}
	}
}
