﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using IdleMiner.GameClasses.CryptoCurrencies;
using Newtonsoft.Json;

namespace IdleMiner.GameClasses
{
	class SaveSlot
	{
        /// <summary>
        /// Название сохранения
        /// </summary>
		public string Name { get; set; }
        /// <summary>
        /// Счет в сохранений
        /// </summary>
		public int Cash { get; set; }
        /// <summary>
        /// Помещения для хранения компьютеров
        /// </summary>
		public List<MiningArea> MiningAreas { get; set; }
        /// <summary>
        /// Криптокошельки содержащее криптокошелек на каждую валюту
        /// </summary>
		public SCryptoWallet[] CryptoWallet { get; set; } = new SCryptoWallet[]
		{
			new SCryptoWallet { CryptoCurrency = new Bitcoin() },
			new SCryptoWallet { CryptoCurrency = new Monero() },
			new SCryptoWallet { CryptoCurrency = new Etherium() },
			new SCryptoWallet { CryptoCurrency = new EtheriumClassic() }
		};
		public SaveSlot(string name, List<MiningArea> areas, int cash = 0)
		{
			Name = name;
			MiningAreas = areas;
			Cash = cash;
		}
		/// <summary>
        /// Записывает этот объект в файл
        /// </summary>
        /// <param name="path"></param>
        /// <returns>True если удачно записалось</returns>
		public bool Save(string path = @"C:\Users\TheWhisperer\source\repos\IdleClicker\SaveSlot.txt")
		{
			try
            {
                using (StreamWriter writer = new StreamWriter(new FileStream(path, FileMode.OpenOrCreate)))
			    {
                    writer.Write(JsonConvert.SerializeObject(this));
                    return true;
			    }
            }
            catch (Exception)
            {
                return false;
            }
		}
		/// <summary>
        /// Загружает сохранение с файла
        /// </summary>
        /// <param name="path"></param>
        /// <returns>Сохранение SaveSlot</returns>
		public static SaveSlot Load(string path = @"C:\Users\TheWhisperer\source\repos\IdleClicker\SaveSlot.txt")
		{
            using (StreamReader reader = new StreamReader(new FileStream(path, FileMode.Open)))
            {
                return (SaveSlot) JsonConvert.DeserializeObject(reader.ReadToEnd());
            }
		}
		/// <summary>
		/// Удаляет файл с сохранением
		/// </summary>
		/// <returns></returns>
		bool Remove()
		{
			return true;
		}
	}
}
