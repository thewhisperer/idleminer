﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdleMiner.GameClasses
{
	class DescTop : Computer
	{
        public DescTop()
		{
			VideoCards = new VideoCard[2];
			switch (Hull)
			{
				case EQuality.NONE:
					break;
				case EQuality.BAD:
					Coolers = new EQuality[2];
					break;
				case EQuality.NORMAL:
					Coolers = new EQuality[3];
					break;
				case EQuality.GOOD:
					Coolers = new EQuality[3];
					break;
				case EQuality.BEST:
					Coolers = new EQuality[4];
					break;
				default:
					break;
			}
		}
	}
}
