﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdleMiner.GameClasses
{
	abstract class Computer
	{
		/// <summary>
		/// Имя компьютера (для различий от других компьютеров)
		/// </summary>
		public string Name { get; set; }
		/// <summary>
		/// Видеокарты компьютера
		/// </summary>
		public VideoCard[] VideoCards { get; protected set; }
		/// <summary>
		/// Корпус компьютера (От этого зависит сколько кулеров можно установить)
		/// </summary>
		public EQuality Hull { get; set; }
		/// <summary>
		/// Кулеры компьютера (Качество зависит сколько тепла кулер может поглотить)
		/// </summary>
		public EQuality[] Coolers { get; protected set; }
		/// <summary>
		/// Целевая криптовалюта, которую необходимо получить
		/// </summary>
		public CryptoCurrency TargetCurrency { get; set; }
		/// <summary>
		/// Добавляет кулер в систему и возвращает добавленный кулер, если есть место. Возвращает EQuality.NONE если мест нет
		/// </summary>
		/// <param name="cooler"></param>
		/// <returns>EQuality cooler</returns>
		public EQuality AddCooler(EQuality cooler)
		{
			for (int i = 0; i < Coolers.Length; i++)
			{
				if (Coolers[i] == EQuality.NONE)
				{
					Coolers[i] = cooler;
					return cooler;
				}
			}
			return EQuality.NONE;
		}
		/// <summary>
		/// Удаляет кулер из системы, возращает кулер если операция удачна, иначе EQuality.NONE
		/// </summary>
		/// <param name="cooler"></param>
		/// <returns>Удаленный кулер</returns>
		public EQuality RemoveCooler(EQuality cooler)
		{
			for (int i = 0; i < Coolers.Length; i++)
			{
				if (Coolers[i] == cooler)
				{
					Coolers[i] = EQuality.NONE;
					return cooler;
				}
			}
			return EQuality.NONE;
		}
		/// <summary>
		/// Добавляет указанную видеокарту в компьютер
		/// </summary>
		/// <param name="videoCard"></param>
		/// <returns>Возвращает добавленную видеокарту</returns>
		public VideoCard AddVideoCard(VideoCard videoCard)
		{
            for (int i = 0; i < VideoCards.Length; i++)
            {
                if (VideoCards[i] == null)
                {
                    VideoCards[i] = videoCard;
                    break;
                }
            }
			return videoCard;
		}
		/// <summary>
		/// Удаляет видеокарту из компьютера
		/// </summary>
		/// <param name="videoCard"></param>
		/// <returns>Возращает удаленную видеокарту в случае удачи, в случае неудачи возвращает null</returns>
		public VideoCard RemoveVideoCard(VideoCard videoCard)
		{
			for (int i = 0; i < VideoCards.Length; i++)
			{
				if (videoCard.Equals(VideoCards[i]))
				{
					VideoCards[i] = null;
					return videoCard;
				}
			}
			return null;
		}
		/// <summary>
		/// Возвращает количество поглощаемого тепла
		/// </summary>
		/// <returns></returns>
		public int ThermoCapacity()
		{
			int thermoCapacity = 0;
			if (Coolers != null)
			{
				foreach (EQuality cooler in Coolers)
				{
					switch (cooler)
					{
						case EQuality.NONE:
							break;
						case EQuality.BAD:
							thermoCapacity += 10;
							break;
						case EQuality.NORMAL:
							thermoCapacity += 25;
							break;
						case EQuality.GOOD:
							thermoCapacity += 50;
							break;
						case EQuality.BEST:
							thermoCapacity += 100;
							break;
						default:
							break;
					}
				}
			}
			switch (Hull)
			{
				case EQuality.NONE:
					break;
				case EQuality.BAD:
					thermoCapacity += 50;
					break;
				case EQuality.NORMAL:
					thermoCapacity += 100;
					break;
				case EQuality.GOOD:
					thermoCapacity += 150;
					break;
				case EQuality.BEST:
					thermoCapacity += 200;
					break;
				default:
					break;
			}
			return thermoCapacity;
		}
	}
}
