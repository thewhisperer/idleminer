﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdleMiner.GameClasses
{
	class MiningArea
	{
		/// <summary>
		/// Тип объекта (гараж, склад, серверная и т.д.)
		/// </summary>
		public string Type { get; set; }
		/// <summary>
		/// Индентификационное имя в списке строений.
		/// Отображается в списке
		/// </summary>
		public string Name { get; set; }
		/// <summary>
		/// Массив компьютеров в помещений
		/// </summary>
		public Computer[] Computers { get; set; }
		/// <summary>
		/// Цена для покупки помещения
		/// </summary>
		public int Price { get; set; }
		/// <summary>
		/// Множитель для кулеров, количество возможных кондиционеров зависит от помещения
		/// </summary>
		public EQuality[] Conditioners { get; set; }

        public MiningArea(string type, string name, int price = 0)
        {
            Type = type;
            Name = name;
            Price = price;
        }
	}
}
