﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IdleMiner
{
	/// <summary>
	/// Логика взаимодействия для MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
		}

		private void ExitBtn_Click(object sender, RoutedEventArgs e) => Close();

		private void OptionBtn_Click(object sender, RoutedEventArgs e)
		{
			Hide();
			Options options = new Options
			{
				Owner = this
			};
			options.Show();
		}

		private void PlayBtn_Click(object sender, RoutedEventArgs e)
		{
			new MainPlay().Show();
			Close();
		}
	}
}
