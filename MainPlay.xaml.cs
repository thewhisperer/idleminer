﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using IdleMiner.GameClasses;
using System.Timers;
using IdleMiner.GameClasses.CryptoCurrencies;

namespace IdleMiner
{
	/// <summary>
	/// Логика взаимодействия для MainPlay.xaml
	/// </summary>
	public partial class MainPlay : Window
	{
		SaveSlot Save { get; set; } = new SaveSlot("Game1", new List<MiningArea>());
		public MainPlay()
		{
            //TODO: Избавиться от классов GTX1050 и заменить одним VideoCard
			InitializeComponent();
			Save.MiningAreas.Add(new MiningArea("Гараж" ,"Стартовый гараж")
			{
				Computers = new Computer[10]
				{
					new DescTop
					{
						Name = "Стартовый ПК",
						Hull = EQuality.BAD,
						TargetCurrency = new Monero(),
					},
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null
				}
			});
			foreach (MiningArea area in Save.MiningAreas)
			{
                if (area == null) continue;
				FarmsList.Items.Add(area);
                foreach (Computer computer in area.Computers)
                {
                    if (computer == null) continue;
                    computer.AddVideoCard(new VideoCard("NVidia Geforce GT 210", 50, 10, 150, 2005));
                }
			}
			DetailsBox.Visibility = Visibility.Hidden;
		}

		private void BackBtn_Click(object sender, RoutedEventArgs e)
		{
            Save.Save();
			new MainWindow().Show();
			Close();
		}

		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
            Timer ticker = new Timer(1000)
			{
				AutoReset = true,
				Enabled = true
			};
			ticker.Elapsed += TickGame;
		}
		/// <summary>
		/// Обновляет данные игры, например заработок в криптовалюте и т.д.
		/// Самая важная функция для программы.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void TickGame(object sender, ElapsedEventArgs e)
		{
			//Update game states
			foreach (MiningArea area in Save.MiningAreas)
			{
				if (area == null) continue;
				foreach (Computer computer in area.Computers)
				{
					if (computer == null) continue;
					int TargetCryptoIndex = 0;
					for (int i = 0; i < Save.CryptoWallet.Length; i++)
					{
						if (Save.CryptoWallet[i].CryptoCurrency.Name == computer.TargetCurrency.Name)
						{
							TargetCryptoIndex = i;
							break;
						}
					}
					foreach (VideoCard videoCard in computer.VideoCards)
					{
						if (videoCard == null) continue;
						Save.CryptoWallet[TargetCryptoIndex].Amount += (int)(computer.TargetCurrency.HashHardness * videoCard.Performance);
					}
				}
			}
			MoneroBlock.Dispatcher.Invoke(() =>{MoneroBlock.Text = $"Monero: {Save.CryptoWallet[1].Amount}";});
		}

		private void ShopBtn_Click(object sender, RoutedEventArgs e)
		{
			new Shop { Owner = this }.ShowDialog();
		}

		private void FarmsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			// Обработчик события на изменение выделения в farmslist.
			//TODO: Дописать логику свойств для компа, по сути нужно сделать так, чтобы при нажатий на ПК, выделялся ПК и в правой окне отображались детали о выделенном ПК
			DetailsBox.Visibility = Visibility.Visible;
			TypeBlock.Text = $"Тип: {((MiningArea)FarmsList.SelectedItem).Type}";
		}

		private void ComputersBtn_Click(object sender, RoutedEventArgs e)
		{
			new ComputersWindow() { Owner = this }.ShowDialog();
		}
	}
}
