﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using IdleMiner.GameClasses;

namespace IdleMiner
{
	/// <summary>
	/// Логика взаимодействия для ComputersWindow.xaml
	/// </summary>
	public partial class ComputersWindow : Window
	{
		public ComputersWindow()
		{
			InitializeComponent();
			ComputerDetailsList.Visibility = Visibility.Hidden;
		}

		private void ComputersList_Loaded(object sender, RoutedEventArgs e)
		{
			foreach (Computer computer in ((MiningArea)(((MainPlay)Owner).FarmsList.SelectedItem)).Computers)
			{
                if (computer == null) continue;
				ComputersList.Items.Add(computer);
			}
		}

		private void BackBtn_Click(object sender, RoutedEventArgs e)
		{
			Close();
		}

		private void ComputersList_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			ComputerDetailsList.Visibility = Visibility.Visible;
			NameBlock.Text = $"Название: {((Computer)ComputersList.SelectedItem).Name}";
			HullBlock.Text = $"Корпус: {((Computer)ComputersList.SelectedItem).Hull.ToString()}";
			ThermoBlock.Text = $"Максимальная емкость тепла: {((Computer)ComputersList.SelectedItem).ThermoCapacity()}";
		}
	}
}
